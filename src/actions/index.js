
export const selectCompany = (company) => {
  
  return {
    type: "SELECTED_COMPANY",
    payload: company,
  };
};

export const selectJob = (job) => {
  
  return {
    type: "SELECTED_JOB",
    payload: job,
  };
};

export const fetchJobs = (job) => {
  
  return {
    type: "FETCH_JOBS",
    payload: job,
  };
};
