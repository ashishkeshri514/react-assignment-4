import { combineReducers } from "redux";
import Jobs from "../jobs.json";

const companiesReducer = () => {
  return [
    {
      name: "Infosys",
      id: 1,
      jobs: [
        {
          title: "React",
          locations: ["Chandigarh", "Delhi", "Noida"],
          salary: "20k to 60k rupees per month",
          jobType: "Full-time",
          requirements: [
            "ES6 is required",
            "React 16 version mut be required",
            "Having experience on Git",
          ],
        },
        {
          title: "Nodejs",
          locations: ["Chandigarh", "Mumbai", "Bangalore"],
          salary: "20k to 60k rupees per month",
          jobType: "Full-time",
          requirements: [
            "ES6 is required",
            "Python experience must required",
            "Having experience on Git",
          ],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
      ],
    },
    {
      name: "TCS",
      id: 2,
      jobs: [
        {
          title: "React",
          locations: ["Chandigarh", "Delhi", "Noida"],
          salary: "20k to 60k rupees per month",
          jobType: "Full-time",
          requirements: [
            "ES6 is required",
            "React 16 version mut be required",
            "Having experience on Git",
          ],
        },
        {
          title: "Nodejs",
          locations: ["Chandigarh", "Mumbai", "Bangalore"],
          salary: "20k to 60k rupees per month",
          jobType: "Full-time",
          requirements: [
            "ES6 is required",
            "Python experience must required",
            "Having experience on Git",
          ],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
      ],
    },
    {
      name: "Talk Valley LLC.",
      id: 3,
      jobs: [
        {
          title: "React",
          locations: ["Chandigarh", "Delhi", "Noida"],
          salary: "20k to 60k rupees per month",
          jobType: "Full-time",
          requirements: [
            "ES6 is required",
            "React 16 version mut be required",
            "Having experience on Git",
          ],
        },
        {
          title: "Nodejs",
          locations: ["Chandigarh", "Mumbai", "Bangalore"],
          salary: "20k to 60k rupees per month",
          jobType: "Full-time",
          requirements: [
            "ES6 is required",
            "Python experience must required",
            "Having experience on Git",
          ],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
        {
          title: "Python",
          locations: ["Mumbai", "Pune", "Hyderabad"],
          salary: "40k to 60k rupees per month",
          jobType: "Full-time",
          requirements: ["Having 5 years of experience"],
        },
      ],
    },
  ];
};
const selectedCompanyReducer = (selectedCompany = null, action) => {
  if (action.type === "SELECTED_COMPANY") {
    return action.payload;
  }

  return selectedCompany;
};
const selectedJobReducer = (selectedJob = null, action) => {
  if (action.type === "SELECTED_JOB") {
    return action.payload;
  }

  return selectedJob;
};

const fetchJobReducer = (jobs = [...Jobs], action) => {
  if (action.type === "FETCH_JOBS") {
    return action.payload;
  }

  return jobs;
};
export default combineReducers({
  companies: companiesReducer,
  selectedCompany: selectedCompanyReducer,
  jobs: fetchJobReducer,
  selectedJob: selectedJobReducer,
});
